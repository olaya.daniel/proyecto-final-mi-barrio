//mapa//
function cargarMapaPrincipal(){
    let mapa= L.map('divMapa',{center:[4.68648554106952,-74.10713911056519], zoom:16});
    let mosaico= L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
        maxZoom: 18, id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

let unMarcador = L.marker([4.685358, -74.106737]);
unMarcador.addTo(mapa);

let poligono= L.polygon([
    [4.689757581063532, -74.1089791059494],
    [4.6894367935063945, -74.10925805568695],
    [4.689263033518097, -74.10941630601883],
    [4.688592052234522, -74.10972744226456],
    [4.687592262194148, -74.10998761653899],
    [4.687025535653631, -74.10920977592468],
    [4.686619203134144, -74.10870015621185],
    [4.68646415513681, -74.10881817340851],
    [4.6863625719474875, -74.1086894273758],
    [4.686036436345132, -74.10901129245758],
    [4.685619410271281, -74.10842657089233],
    [4.685680894900361, -74.1083112359047],
    [4.683643879528231, -74.10560488700867],
    [4.6851756514835685, -74.10443007946014],
    [4.686367918431498, -74.10555124282837],
    [4.687870278819574, -74.10677969455719],
    [4.688581359300482, -74.10741806030273],
    [4.6893191713652005, -74.10841584205626],
    [4.689757581063532, -74.1089791059494]
])
poligono.addTo(mapa);
}

//mapa vóleibol//

var mapaDos;
function cargarMapaVoleibol(){
     mapaDos= L.map('divMapaDos',{center:[4.68705761452668, -74.10828441381454], zoom:16});
    let mosaicoDos = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
     maxZoom: 18,  id: 'mapbox.streets'
    });
     mosaicoDos.addTo(mapaDos);    
     let marcador= L.marker([4.68705761452668, -74.10828441381454]);
     marcador.addTo(mapaDos);
}

//mapa micro//
var mapaMicro;
function cargarMapaMicro(){
     mapaMicro= L.map('divMapaMicro',{center:[4.68648554106952,-74.10713911056519], zoom:16});
    let mosaicoMicro= L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
        maxZoom: 18, id: 'mapbox.streets'
    });
    mosaicoMicro.addTo(mapaMicro);

let unMarcadorMicro = L.marker([4.6881376023935335, -74.10713911056519]);
unMarcadorMicro.addTo(mapaMicro);
}

//mapa sitios de interés//
 

function cargarMapaSitios(){
    let mapaSitios= L.map('divMapaSitios',{center:[4.68648554106952,-74.10713911056519], zoom:16});
    let mosaicoSitios= L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
        maxZoom: 18, id: 'mapbox.streets'
    });
    mosaicoSitios.addTo(mapaSitios);

let unMarcadorSitios = L.marker([4.686522966450192, -74.1057014465332]).bindPopup("Talleres");
unMarcadorSitios.addTo(mapaSitios);

let unMarcadorSitios2 =L.marker([4.684437835044379, -74.1051059961319]).bindPopup("Tiendas y Restaurantes");
unMarcadorSitios2.addTo(mapaSitios);

let unMarcadorSitios3= L.marker([4.687159197614975, -74.1085284948349]).bindPopup("Parques");
unMarcadorSitios3.addTo(mapaSitios);

let unMarcadorSitios4= L.marker([4.686736825729877, -74.10622179508208]).bindPopup("Plazas, supermercados y famas");
unMarcadorSitios4.addTo(mapaSitios);

let unMarcadorSitios5= L.marker([4.683750809616601, -74.10574972629547]).bindPopup("Dorguerías, Peluquerías y Barberías");
unMarcadorSitios5.addTo(mapaSitios);

let poligono2= L.polygon([
    [4.68509545408224,-74.10450518131256],
    [4.685186344469719,-74.10441398620605],
    [4.6868865271867,-74.10594284534454],
    [4.68849581582228,-74.10736441612244],
    [4.689543722708485,-74.10871088504791],
    [4.6896239195992955,-74.10918831825256],
    [4.689137391653476,-74.10965502262115],
    [4.687725924047099,-74.11010563373566],
    [4.687597608668757,-74.10986423492432],
    [4.6890251159255705,-74.10929560661316],
    [4.689420754124665,-74.10890936851501],
    [4.688191067096051,-74.1073215007782],
    [4.686972070861931,-74.10623252391815],
    [4.685982971477785,-74.10543859004974],
    [4.68509545408224,-74.10450518131256]
], {
    color: 'black',
    fillColor: 'blue',
    fillOpacity: 0.4,

})
poligono2.addTo(mapaSitios);

let poligono3= L.polygon([
    [4.686081881479192,-74.10529911518097],
    [4.685972278503823,-74.10539299249649],
    [4.685119513303602,-74.10458832979202],
    [4.684501993026523,-74.10504430532455],
    [4.684066253282032,-74.10538762807846],
    [4.683702691078856,-74.10569876432419],
    [4.68364922603303,-74.10559684038161],
    [4.684154470552839,-74.10517305135726],
    [4.684721199421089,-74.104765355587],
    [4.684953771984086,-74.10457491874695],
    [4.685180997976663,-74.10441666841507],
    [4.686081881479192,-74.10529911518097]

], {
    color: 'purple',
    fillColor: 'violet',
    fillOpacity: 0.8,
})
poligono3.addTo(mapaSitios);

let poligono4= L.polygon([
    [4.686349205737263,-74.10838097333908],
    [4.686589797482115,-74.10822808742523],
    [4.686718113045514,-74.10837292671204],
    [4.687095039876729,-74.1080591082573],
    [4.687295532789222,-74.10826295614243],
    [4.6880360194474715,-74.10768896341324],
    [4.688346114710216,-74.10738587379456],
    [ 4.6887203674302365,-74.1078069806099],
    [4.686974744101614,-74.1091239452362],
    [4.686632569339202,-74.10871088504791],
    [4.686453462170201,-74.10882353782654],
    [4.6863625719474875,-74.10870552062988],
    [4.686020396885365,-74.1089978814125],
    [4.685611390536658,-74.10845607519148],
    [4.686065842020442,-74.10803630948067],
    [4.686349205737263,-74.10838097333908]

],{
    color: 'green',
    fillColor: 'green',
    fillOpacity: 0.3,
})
poligono4.addTo(mapaSitios);


let poligono5= L.polygon([
    [4.687004149737447,-74.10617351531982],
    [4.686637915821152,-74.10659998655319],
    [4.686432076236511,-74.10644441843033],
    [4.686768904616182,-74.10596966743469],
    [4.687004149737447,-74.10617351531982]

],{
    color: 'gray',
    fillColor: 'black',
    fillOpacity: 0.3,
})
poligono5.addTo(mapaSitios);

let poligono6= L.polygon([
   [4.685849309291488,-74.10513818264008],
   [4.685688914634195,-74.10526692867279],
   [4.6851542655107705,-74.10471439361572],
   [4.684897633786308,-74.10488605499268],
   [4.684801396865342,-74.10476803779602],
   [4.6851756514835685,-74.1044569015503],
   [4.685849309291488,-74.10513818264008]

],{
    color: 'gray',
    fillColor: 'black',
    fillOpacity: 0.3,
})
poligono6.addTo(mapaSitios);

let poligono7= L.polygon([
    [4.684218628560974,-74.10508990287781],
    [4.684245361062642,-74.10516500473022],
    [4.684092985789513,-74.10529643297195],
    [4.684616942729801,-74.10601794719696],
    [4.684571497500528,-74.10610377788544],
    [4.683970016246642,-74.10529106855392],
    [4.684218628560974,-74.10508990287781]
],{
    color: 'orange',
    fillColor: 'red',
    fillOpacity: 0.3,
})
poligono7.addTo(mapaSitios);

let poligono8= L.polygon([
    [4.687624341041192,-74.10920977592468],
    [4.687421174985067,-74.10893619060516],
    [4.687079000441248,-74.1091775894165],
    [4.6870362286114755,-74.10907030105591],
    [4.68744256088849,-74.10880208015442],
    [4.687741963467733,-74.10921514034271],
    [4.687624341041192,-74.10920977592468]
],{
    color: 'orange',
    fillColor: 'red',
    fillOpacity: 0.3,
})
poligono8.addTo(mapaSitios);

poligono9= L.polygon([
    [4.68507941460087,-74.10510465502739],
    [4.68503396940166,-74.10512745380402],
    [4.684723872669386,-74.10477876663208],
    [4.684766644640629,-74.10475328564644],
    [4.68507941460087,-74.10510465502739]
],{
    color: 'orange',
    fillColor: 'red',
    fillOpacity: 0.3,
})
poligono9.addTo(mapaSitios);

let poligono10= L.polygon([
    [4.683836353675522,-74.10578191280365],
    [4.683761502624533,-74.10585969686508],
    [4.683606453993429,-74.10563170909882],
    [4.68367863180873,-74.10556197166443],
    [4.683836353675522,-74.10578191280365]
],{
    color: 'orange',
    fillColor: 'red',
    fillOpacity: 0.3,
})
poligono10.addTo(mapaSitios);
}
//formularios/

function guardarDatosVoleibol(){
    localStorage.apellido= document.getElementById("apellidoVoleibol").value;
    localStorage.nombre= document.getElementById("nombreVoleibol").value;
    localStorage.fecha= document.getElementById("fechaVoleibol").value;
    localStorage.correo= document.getElementById("correoVoleibol").value;
    localStorage.numero= document.getElementById("numeroVoleibol").value;
}

function guardarDatosMicro(){
    localStorage.apellido= document.getElementById("apellidoMicro").value;
    localStorage.nombre= document.getElementById("nombreMicro").value;
    localStorage.fecha= document.getElementById("fechaMicro").value;
    localStorage.correo= document.getElementById("correoMicro").value;
    localStorage.numero= document.getElementById("numeroMicro").value;
}